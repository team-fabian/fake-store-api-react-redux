import "./App.css";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchProducts } from "./service/Actions/Products";
import Navbar from "./components/Navbar/Navbar";
import Productcard from "./components/Productcard/Productcard";

function App() {
  const dispatch = useDispatch();
  const products = useSelector((state) => state.products.products);
  const loading = useSelector((state) => state.products.loading);
  const error = useSelector((state) => state.products.error);

  useEffect(() => {
    dispatch(fetchProducts());
  }, [dispatch]);

  const allProducts = products.map((product) => {
    return <Productcard key={product.id} data={product} />;
  });

  return (
    <div>
      {loading ? (
        <p>Loading...</p>
      ) : error ? (
        <p>{error}</p>
      ) : (
        <>
          <Navbar />
          <div className="allProducts">{allProducts}</div>
        </>
      )}
    </div>
  );
}

export default App;
