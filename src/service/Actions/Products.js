import axios from 'axios';
import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  loading: false,
  products: [],
  error: ''
};

const productsSlice = createSlice({
  name: 'products',
  initialState,
  reducers: {
    fetchProductsRequest: (state) => {
      state.loading = true;
    },
    fetchProductsSuccess: (state, action) => {
      state.loading = false;
      state.products = action.payload;
      state.error = '';
    },
    fetchProductsFailure: (state, action) => {
      state.loading = false;
      state.products = [];
      state.error = action.payload;
    }
  }
});

export const { fetchProductsRequest, fetchProductsSuccess, fetchProductsFailure } = productsSlice.actions;

export const fetchProducts = () => {
  return (dispatch) => {
    dispatch(fetchProductsRequest());

    axios.get('https://fakestoreapi.com/products')
      .then(response => {
        const products = response.data;
        dispatch(fetchProductsSuccess(products));
      })
      .catch(error => {
        const errorMessage = error.message;
        dispatch(fetchProductsFailure(errorMessage));
      });
  };
};

export default productsSlice.reducer;
