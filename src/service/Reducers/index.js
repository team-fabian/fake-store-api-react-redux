import { combineReducers } from 'redux';
import productsReducer from '../Actions/Products'
const rootReducer = combineReducers({
  products: productsReducer
});

export default rootReducer;
