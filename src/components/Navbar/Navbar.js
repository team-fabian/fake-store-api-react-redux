import React from 'react'
import "./Navbar.css"

export default function Navbar() {
  return (
    <>
      <div className="navbar">
        <div className="brand">
          <i className="fa-solid fa-shirt"></i>
          Myntra
        </div>
        <div className="allButtons">
          {/* <Link to={"/cart"}> */}
            <button className="cartButton">
              <i class="fa-solid fa-cart-shopping"></i>Cart
            </button>
          {/* </Link> */}
        </div>
      </div>
    </>
  );
}
